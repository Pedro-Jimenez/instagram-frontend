ScriptPost
const contenedorPost = document.getElementById('contenedor-post');
const urlAPI = 'https://gkfibffviwvmphzqvuqe.supabase.co/storage/v1/object/public/fci-personal'; 

contenedorPost.addEventListener('submit', async (event) => {
  event.preventDefault();

  const formData = new FormData(contenedorPost);
  const contenido = formData.get('contenido');

  try {
    const response = await fetch(urlAPI, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'apikey': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdrZmliZmZ2aXd2bXBoenF2dXFlIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA5ODQ5NTgsImV4cCI6MjAyNjU2MDk1OH0.M--1JO0f0zos59CcBc8oCPKZmz2su3qx0Z2hOqQK9c0',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdrZmliZmZ2aXd2bXBoenF2dXFlIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA5ODQ5NTgsImV4cCI6MjAyNjU2MDk1OH0.M--1JO0f0zos59CcBc8oCPKZmz2su3qx0Z2hOqQK9c0'
        
      },
      body: JSON.stringify({ contenido: contenido }),
    });

    if (!response.ok) {
      throw new Error('Error al crear el post');
    }

    const responseData = await response.json();
    console.log('Post creado:', responseData);
    // Aquí podrías hacer algo más, como mostrar un mensaje de éxito o redirigir al usuario a otra página
  } catch (error) {
    console.error('Error al crear el post:', error);
    // Aquí podrías mostrar un mensaje de error al usuario
  }
});
